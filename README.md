# Google protobuf definitions

## Install
Install protoc from source or package manager.

Next install the golang protoc generators:
`go get google.golang.org/grpc/cmd/protoc-gen-go-grpc`  
`go install google.golang.org/grpc/cmd/protoc-gen-go-grpc`  

Finally in your bashrc:  
`export $PATH=${GOPATH-$HOME/.local/share/go}/bin`
